# Description:
* For Android

# Prerequisite:
1.    Python 2.7/3.x installed
2.    Appium installed
3.    Selenium installed
4.    Adb installed
5.    Behave installed

`pip install git+https://github.com/behave/behave`

    
# How to run:
1.    Configuration:

* Edit `config_vars.py` file under `~/hkobservatory/features`
* Make sure following variable is correct according to your machine and device:
    * appiumHub
    * platformName
    * platformVersion
    * udid
    
2.    Save `config_vars.py` after edit complete
3.    Run `run_test.sh` under `~/hkobservatory`
4.    A Junit report will be generated under report according to running timestamp