from features.pages.disclaimer import DisclaimerPage
from features.pages.homepage import HomePage
from features.pages.menu import Menu
from features.pages.weatherForecast import WeatherForecast

class Application:
    def __init__(self, driver):
        self.disclaimer_page = DisclaimerPage(driver)
        self.home_page = HomePage(driver)
        self.menu = Menu(driver)
        self.weatherForecast_page = WeatherForecast(driver)