from selenium.common import exceptions
from selenium.webdriver.common import *
from appium.webdriver.common.touch_action import TouchAction
from selenium.webdriver.support.wait import WebDriverWait


class AppAction:
    def __init__(self, driver):
        self.driver = driver

    #def find_element(self, locator):
    #    return self.driver.find_element(locator[0], locator[1])

    def find_element(self, locator, timeout=5, poll_freq=0.5):
        try:
            element = WebDriverWait(self.driver, timeout, poll_freq).until(lambda x: x.find_element(locator[0], locator[1]))
            return element
        except (exceptions.NoSuchElementException, exceptions.TimeoutException) as e:
            print('element not found')
            return False

    def click_on_element(self, locator):
        element = self.find_element(locator)
        element.click()

    def scroll_down_to_element(self, locator):
        element = self.find_element(locator,timeout=1)
        if not element:
            print("scroll: cannot find element")


    def element_is_visible(self, locator, timeout=10, poll_freq=0.5):
            if(self.find_element(locator, timeout, poll_freq)):
                return True
            else:
                return False

    def scroll_by_percent(self, x1, y1, x2, y2):
        h = self.driver.window_size['height']
        w = self.driver.window_size['width']
        n_x1 = w*x1/100
        n_x2 = w*x2/100
        n_y1 = h*y1/100
        n_y2 = h*y2/100

        actions = TouchAction(self.driver)
        actions.long_press(x=n_x1,y=n_y1,duration=10).move_to(x=n_x2,y=n_y2).release().perform()
        #self.driver.TouchAction().press(n_x1,n_y1).moveTo(n_x2,n_y2).release()

    def element_is_selected(self, locator):
        return self.find_element(locator).get_attribute('selected')