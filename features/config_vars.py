#config variables

appiumHub = 'http://localhost:4723/wd/hub'
desired_capabilities={
            'platformName': 'Android',
            'platformVersion': '10',
            'udid': '8C8Y15FXJ',
            'appPackage': 'hko.MyObservatory_v1_0',
            'appActivity': '.AgreementPage',
            'noReset': 'false',
            'automationName': 'UiAutomator2',
            'autoGrantPermissions': 'true'
        }