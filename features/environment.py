from appium import webdriver
from app.app import Application
from config_vars import *
from behave import *
import time
from selenium.webdriver.common.by import By

@fixture
def before_all(context):
    context.config.setup_logging()
    context.driver = webdriver.Remote(appiumHub,
                                      desired_capabilities=desired_capabilities
                                      )
    context.driver.implicitly_wait(10)
    context.app = Application(context.driver)
    context.driver.start_activity(desired_capabilities['appPackage'], desired_capabilities['appActivity'])
    context.driver.window_size = context.driver.get_window_size()
    #context.driver.reset()

def after_all(context):
    context.driver.quit()