from selenium.webdriver.common.by import By
from features.commonAction import AppAction

class DisclaimerPage(AppAction):
    DisclaimerTitle = [By.XPATH, "//android.widget.TextView[@text=\"Disclaimer\"]"]
    PolicyTitle = [By.XPATH, "//android.widget.TextView[@text=\"Privacy Policy Statements\"]"]
    Agree_btn = [By.ID, "btn_agree"]

    def disclaimer_title_is_visible(self):
        return self.element_is_visible(self.DisclaimerTitle)

    def agree_disclaimer(self):
        self.click_on_element(self.Agree_btn)

    def policy_title_is_visible(self):
        return self.element_is_visible(self.PolicyTitle)