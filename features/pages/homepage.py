from selenium.webdriver.common.by import By
from selenium.common import exceptions
from features.commonAction import AppAction
import time

class HomePage(AppAction):
    ## What's New
    WhatsNew_Container = [By.ID, "view_pager"]
    WhatsNew_SkipBtn = [By.ID, "btn_friendly_reminder_skip"]

    ## Home page elements
    Home_Temp_Layout = [By.ID, "right_temp_layout"]

    def whatsnew_is_visible(self):
        return self.element_is_visible(self.WhatsNew_Container,timeout=5)

    def skip_whatsnew(self):
        self.click_on_element(self.WhatsNew_SkipBtn)

    def skipAll_whatsnew(self):
        while(self.whatsnew_is_visible()):
            print('still on what\'s new')
            self.skip_whatsnew()
            time.sleep(2)

    def home_temp_is_visible(self):
        return self.element_is_visible(self.Home_Temp_Layout, timeout=5)