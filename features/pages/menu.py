from features.commonAction import AppAction
from selenium.webdriver.common.by import By

class Menu(AppAction):
    Menu_Btn = [By.XPATH, "//android.widget.ImageButton[@content-desc=\"Navigate up\"]"]
    Menu_NineDayForecast_Btn = [By.XPATH,
                           "//android.widget.LinearLayout[./*/android.widget.TextView[@text=\"9-Day Forecast\"]]"]
    Menu_Home_btn = [By.ID, "home_page"]

    def open_menu(self):
        self.click_on_element(self.Menu_Btn)

    def homeBtn_is_visible(self):
        return self.element_is_visible(self.Menu_Home_btn,timeout=5)

    def ninedayforecastBtn_is_visible(self):
        return self.element_is_visible(self.Menu_NineDayForecast_Btn)

    def scroll_to_in_menu(self, target):
        while(not self.element_is_visible(target,timeout=5)):
            self.scroll_by_percent(50,50,50,20)

    def scroll_to_ninedayforecast_in_menu(self):
        self.scroll_to_in_menu(self.Menu_NineDayForecast_Btn)

    def tap_menuItem(self, menuItem):
        menuItem = menuItem.lower()
        if(menuItem=='9-day forecast'):
            self.click_on_element(self.Menu_NineDayForecast_Btn)