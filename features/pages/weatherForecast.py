from features.commonAction import AppAction
from selenium.webdriver.common.by import By

class WeatherForecast(AppAction):
    WeatherForecast_PageTitle = [By.XPATH, "//android.view.ViewGroup/android.widget.TextView[@text=\"Weather Forecast\"]"]
    NineDayForecast_Tab = [By.XPATH, "//*[@content-desc=\"9-Day Forecast\"]"]

    def weatherForecast_title_is_visible(self):
        return self.element_is_visible(self.WeatherForecast_PageTitle)

    def nineDayForecast_tab_is_selected(self):
        return self.element_is_selected(self.NineDayForecast_Tab)