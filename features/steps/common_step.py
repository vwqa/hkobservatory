from behave import *

@given('I launch app')
def launch_app(context):
    pass

@then('I see disclaimer page')
def see_disclaimer(context):
    assert context.app.disclaimer_page.disclaimer_title_is_visible() is True

@when('i tap agree button')
def agree_disclaimer(context):
    context.app.disclaimer_page.agree_disclaimer()

@then('i see privacy page')
def see_privacy(context):
    assert context.app.disclaimer_page.policy_title_is_visible() is True

@then('i see whats new page')
def whatsnew_container(context):
    assert context.app.home_page.whatsnew_is_visible() is True

@When('i skip all whats new page')
def skip_all_whatsnew(context):
    context.app.home_page.skipAll_whatsnew()

@Step('i am at home page')
def see_homepage(context):
    assert context.app.home_page.home_temp_is_visible() is True