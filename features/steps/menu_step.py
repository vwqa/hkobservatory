from behave import *

@when('i tap menu button')
def tap_menuBtn(context):
    context.app.menu.open_menu()

@then('i see menu home button')
def menuHomeBtn_is_visible(context):
    assert context.app.menu.homeBtn_is_visible() is True

@when('i scroll to "{menuItem}"')
def menu_scroll_to_item(context, menuItem):
    menuItem=menuItem.lower()
    if(menuItem == "9-day forecast"):
        context.app.menu.scroll_to_ninedayforecast_in_menu()

@step('i tap "{menuItem}"')
def see_menuItem(context, menuItem):
    menuItem = menuItem.lower()
    context.app.menu.tap_menuItem(menuItem)