from behave import *

@step('i see weather forecast title')
def see_weather_forecast_title(context):
    context.app.weatherForecast_page.weatherForecast_title_is_visible()

@step('i see "{forecast_type}" tab is selected')
def forecast_type_is_selected(context, forecast_type):
    forecast_type = forecast_type.lower()
    if(forecast_type=="9-day forecast"):
        context.app.weatherForecast_page.nineDayForecast_tab_is_selected()