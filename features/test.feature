Feature: Test run

  Scenario: Launch app and accept disclaimer
    Given i launch app
    Then i see disclaimer page
    When i tap agree button
    Then i see privacy page
    When i tap agree button
    Then i see whats new page
    When i skip all whats new page
    Then i am at home page

  Scenario: Go to 9 Day Forecast Page
    Given i am at home page
    When i tap menu button
    Then i see menu home button
    When i scroll to "9-day forecast"
    And i tap "9-day forecast"
    Then i see weather forecast title
    And i see "9-day forecast" tab is selected